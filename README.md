### Overview ###

This is a simple project in order to allow more complex meta programming in C/C++. The utility generates code, which should be included in the relavent .cpp file, which will allow more advanced introspection than C/C++ currently allows.

### Build instructions ###

Windows: Simple call the build.bat file from the command line (after calling vcvarsall.bat). This will generate the .exe, which should be completely portable (not even requiring the win32 crt).

Linux: Instructions are very similar, but sadly I haven't had time to properly test them yet.

Mac: N/A.

### Contact ###

Any bugs, suggestions, complains, or just general feedback, should be emailed to seagull127@ymail.com.

### LICENSE ###
This software is dual-licensed to the public domain and under the following license: you are granted a perpetual, irrevocable license to copy, modify, publish, and distribute this file as you see fit.